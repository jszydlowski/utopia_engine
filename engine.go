package main

import (
	ue "bitbucket.org/jszydlowski/utopia_engine/engine"
	//"fmt"
)


func main() {
	var game *ue.Game;
	game = ue.NewGame()

	var cmd1 ue.RestCommand
	var cmd2 ue.SearchCommand
	//var cmd ue.Command

	//fmt.Println(cmd1.Log())
	//fmt.Println(cmd2.Log())

	game.ExecuteCmd(cmd1)
	game.ExecuteCmd(cmd2)

	game.DebugPrint()

}

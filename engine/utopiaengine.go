package engine

import "fmt"

const MAX_HP int = 6
const DEFAULT_DOOMSDAY int = 15

type State int
type SearchState int

const (
	STATE_NEW_GAME State = iota
	STATE_THE_END
	STATE_UNCONSCIUSNESS
)

const (
	SEARCH_NOSEARCH SearchState = iota
	SEARCH_START
)

type Game struct {
	stores				[4]int
	hp					int
	daysPassed			int
	doomsDay			int
	state				State
	searchState			SearchState
	searchTables		[6][6]SearchTable

	commandList			[]Command
}

func NewGame() *Game {
	var game Game

	game.stores = [4]int{0, 0, 0, 0}
	game.hp = 6
	game.daysPassed = 0
	game.doomsDay = DEFAULT_DOOMSDAY
	game.state = STATE_NEW_GAME
	game.searchState = SEARCH_NOSEARCH

	return &game
}

func (g Game) DebugPrint() {
	fmt.Println("[DEBUG PRINT] Start")
	fmt.Printf("Days Passed: %d, HP: %d\n", g.daysPassed, g.hp)
	fmt.Printf("Stores: %+v\n", g.stores)


	fmt.Println("[DEBUG PRINT] End")
}

func (g Game) NextDay() {
	g.daysPassed++
	if g.daysPassed >= g.doomsDay {

	}
}

func (g Game) ActionRest() {
	g.RestoreOneHP()
	g.NextDay()
}

func (g Game) RestoreOneHP() {
	if g.hp < MAX_HP {
		g.hp++
	}
}

func (g Game) UnconsciousnessRest() {
	if g.hp == 0 {
		for i := 0; i < 6; i++ {
			g.ActionRest()
		}
	}
}

package engine

import (
	"fmt"
)

type Location int
const (
	WORKSHOP Location = iota
)

type Command interface {
	Validate() bool
	Log() string
//	serialize() []byte
}


func (g Game) AppendCommandList (cmd Command) {
	if g.commandList == nil {
		g.commandList = make(
}

func (g Game) ExecuteCmd(cmd Command) bool {
	valid := cmd.Validate()
	if valid == true {
		switch t := cmd.(type) {
		case SearchCommand:
			return g.ExecuteSearchCommand(t)
		default:
			fmt.Printf("Unknown Command type = %T\n", cmd )
			return false
		}
	}
}

package engine

type Stats struct {
	turns int
	rolls [6]int

	combat_count int
	combat_hp_lost int
	total_hp_lost int
}


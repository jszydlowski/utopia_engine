package engine

type SearchCommand struct {
	location Location
	dice1pos int
	dice1val int
	dice2pos int
	dice2val int
}

func (g Game) ExecuteSearchCommand(cmd SearchCommand) bool {
	return false
}

func (c SearchCommand) Validate() bool {
	if c.dice1pos < 1 || c.dice1pos > 6 {return false}
	if c.dice2pos < 1 || c.dice2pos > 6 {return false}
	if c.dice1val < 1 || c.dice1val > 6 {return false}
	if c.dice2val < 1 || c.dice2val > 6 {return false}
	if c.location == WORKSHOP {return false}

	return true
}

func (c SearchCommand) Log() string {
	if c.Validate() == true {
		return "Now searching."
	} else {
		return "Invalid Search Command parameters."
	}
}



package engine

type RestCommand struct {
}

func (c RestCommand) Validate() bool {
	// No parameters needed so always return true
	return true
}

func (c RestCommand) Log() string {
	return "Now resting."
}
